import  axios from 'axios'

const base_url = 'https://jsonplaceholder.typicode.com/'
const usersData =[
    {
      "id": 1,
      "name": "Leanne Graham",
      "username": "Bret",
      "email": "Sincere@april.biz",
      "address": {
        "street": "Kulas Light",
        "suite": "Apt. 556",
        "city": "Gwenborough",
        "zipcode": "92998-3874",
        "geo": {
          "lat": "-37.3159",
          "lng": "81.1496"
        }
      },
      "phone": "1-770-736-8031 x56442",
      "website": "hildegard.org",
      "company": {
        "name": "Romaguera-Crona",
        "catchPhrase": "Multi-layered client-server neural-net",
        "bs": "harness real-time e-markets"
      }
    },
    {
      "id": 2,
      "name": "Ervin Howell",
      "username": "Antonette",
      "email": "Shanna@melissa.tv",
      "address": {
        "street": "Victor Plains",
        "suite": "Suite 879",
        "city": "Wisokyburgh",
        "zipcode": "90566-7771",
        "geo": {
          "lat": "-43.9509",
          "lng": "-34.4618"
        }
      }
    }
]

const getUsers = async () => {
    const usersPromise = await  axios.get(base_url+'users')
    const users = usersPromise.data.map(async user => {
        
      const postsPromise = await axios.get(base_url+'users/'+user.id+'/posts')
      const posts=  postsPromise.data.map(async post => {
        const comments = await axios.get(base_url+'posts/'+post.id+'/comments')
        return {...post, comments: comments.data }
      })
      const albumsPromise = await axios.get(base_url+'users/'+user.id+'/albums')
      const albums = albumsPromise.data.map(async album => {
        const photos = await axios.get(base_url+'albums/'+album.id+'/photos')
        return {...album, photos: photos.data}
      })
      return { ...user, posts: posts, albums: albums}
    })
    return users
}

const getUser =  async (root, args) => {
    const user =  await  axios.get(base_url+'users/'+args.id)
    const postsPromise = await axios.get(base_url+'users/'+user.data.id+'/posts')

    const posts=  postsPromise.data.map(async post => {
      const comments = await axios.get(base_url+'posts/'+post.id+'/comments')
      return {...post, comments: comments.data }
    })
    const albumsPromise = await axios.get(base_url+'users/'+user.data.id+'/albums')
    const albums = albumsPromise.data.map(async album => {
        const photos = await axios.get(base_url+'albums/'+album.id+'/photos')
        return {...album, photos: photos.data}
    })
    return {...user.data , posts: posts, albums: albums}  
}

const getPosts = async () => {
  const postsPromise = await axios.get(base_url+'posts')
  return postsPromise.data
}

const getPost = async (root, args) => {
  const post = await axios.get(base_url+'posts/'+args.id)
  return post.data
}

const getComments = async () => {
  const commentsPromise = await axios.get(base_url+'comments')
  return commentsPromise.data
}

const getComment = async (root, args) => {
  const comment = await axios.get(base_url+'comments/'+args.id)
  return comment.data
}

const getAlbums = async () => {

    const albumsPromise = await axios.get(base_url+'albums')
    const albums = albumsPromise.data.map(async album => {
        const photos = await axios.get(base_url+'albums/'+album.id+'/photos')
        return {...album, photos: photos.data}
    })
    return albums
}
  
const getAlbum = async (root, args) => {
    const album = await axios.get(base_url+'album/'+args.id)
    const photos = await axios.get(base_url+'albums/'+album.data.id+'/photos')
    return {...album.data, photos: photos.data}
  }

  const getPhotos = async () => {
    const photosPromise = await axios.get(base_url+'photos')
    return photosPromise.data
  }
  
  const getPhoto = async (root, args) => {
    const photo = await axios.get(base_url+'photos/'+args.id)
    return photo.data
  }

  const createPost = async (root, args) => {
    console.log(args);
    const postPromise = await axios.post(base_url+'posts', {
      userId: args.id,
      title: args.title,
      body: args.body
    })
    console.log(postPromise.data)
    return postPromise.data
  }
  const deletePost = async (root, args) => {
    const postPromise = await axios.delete(base_url+'posts/'+args.id)
    return postPromise.data
  }
  const updatePost = async (root, args) => {
    const post = await axios.put(base_url+'posts/'+args.id, {
      title: args.title,
      body: args.body
    })
    return post.data
  }

export const resolversApi = { getUsers, getUser, getPosts, getPost,
     getComments, getComment, getPhotos, getPhoto, getAlbums, getAlbum,
     createPost, deletePost, updatePost
}