export const typeDefs = `
  type Geo {
    lat: String,
    lng: String
  }

  type Address {
    street: String,
    suite: String,
    city: String,
    zipcode: String,
    geo: Geo
  }

  type Company {
    name: String,
    catchPhrase: String
    bs: String
  }

  type User {
    id: Int!
    name: String,
    username: String,
    email: String,
    address: Address,
    phone: String,
    website: String,
    company: Company,
    posts: [Post]
    albums: [Album]
  }

  type Album {
    userId: Int!
    id: Int!
    title: String
    photos: [Photo]
  }

  type Photo {
    albumId: Int!
    id: Int!
    title: String
    url: String
    thumbnailUrl: String
  }

  type Comment {
    postId: Int!,
    id: Int!,
    name: String,
    email: String,
    body: String
  }

  type Post {
    userId: Int!,
    id: Int!,
    title: String,
    body: String,
    comments: [Comment]
  }
  

  type Query {
    users: [User]
    user(id: Int!): User
    posts: [Post]
    post(id: Int!): Post
    comments: [Comment]
    comment(id: Int!): Comment
    albums: [Album]
    album(id: Int!): Album
    photos: [Photo]
    photo(id: Int!): Photo
  }

  type Mutation {
    createPost(userId: Int!, title: String!, body: String!): Post
    deletePost(id: Int!, ): Post
    updatePost(id: Int!, title: String!, body: String!): Post
  }

`;
