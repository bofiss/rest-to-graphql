import * as  express from 'express'
import * as bodyParser from 'body-parser';
import { graphqlExpress, graphiqlExpress } from 'apollo-server-express';
import { makeExecutableSchema } from 'graphql-tools'
import { resolversApi } from './resolvers'
import { typeDefs } from './schema';

const helperMiddleware = [
    bodyParser.json(),
    bodyParser.text({ type: 'application/graphql' }),
    (req, res, next) => {
        if (req.is('application/graphql')) {
            req.body = { query: req.body };
        }
        next();
    }
];




// The resolvers
const resolvers = {
    Query: { 
        users: resolversApi.getUsers,
        user:  (root, args) => resolversApi.getUser(root, args),
        posts: resolversApi.getPosts,
        post: (root, args) => resolversApi.getPost(root, args),
        comments: resolversApi.getComments,
        comment: (root, args) => resolversApi.getComment(root, args),
        albums: resolversApi.getAlbums,
        album: (root, args) => resolversApi.getAlbum(root, args),
        photos: resolversApi.getPhotos,
        photo: (root, args) => resolversApi.getPhoto(root, args),
    },
    Mutation: {
        createPost:(root, args ) => resolversApi.createPost(root, args),
        updatePost:(root, args ) => resolversApi.createPost(root, args),
        deletePost:(root, args ) => resolversApi.createPost(root, args)
    }
  };

 
const schema = makeExecutableSchema({
    typeDefs,
    resolvers
})

let app = express()
app.use('/graphql',...helperMiddleware, graphqlExpress({
    schema
}))
app.get('/graphiql', graphiqlExpress({ endpointURL: '/graphql' })); // if you want GraphiQL enabled
app.listen(5000, () => {
    console.log("running on port 5000")
})