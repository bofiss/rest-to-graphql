"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express = require("express");
const bodyParser = require("body-parser");
const apollo_server_express_1 = require("apollo-server-express");
const graphql_tools_1 = require("graphql-tools");
const resolvers_1 = require("./resolvers");
const schema_1 = require("./schema");
const helperMiddleware = [
    bodyParser.json(),
    bodyParser.text({ type: 'application/graphql' }),
    (req, res, next) => {
        if (req.is('application/graphql')) {
            req.body = { query: req.body };
        }
        next();
    }
];
// The resolvers
const resolvers = {
    Query: {
        users: resolvers_1.resolversApi.getUsers,
        user: (root, args) => resolvers_1.resolversApi.getUser(root, args),
        posts: resolvers_1.resolversApi.getPosts,
        post: (root, args) => resolvers_1.resolversApi.getPost(root, args),
        comments: resolvers_1.resolversApi.getComments,
        comment: (root, args) => resolvers_1.resolversApi.getComment(root, args),
        albums: resolvers_1.resolversApi.getAlbums,
        album: (root, args) => resolvers_1.resolversApi.getAlbum(root, args),
        photos: resolvers_1.resolversApi.getPhotos,
        photo: (root, args) => resolvers_1.resolversApi.getPhoto(root, args),
    },
    Mutation: {
        createPost: (root, args) => resolvers_1.resolversApi.createPost(root, args),
        updatePost: (root, args) => resolvers_1.resolversApi.createPost(root, args),
        deletePost: (root, args) => resolvers_1.resolversApi.createPost(root, args)
    }
};
const schema = graphql_tools_1.makeExecutableSchema({
    typeDefs: schema_1.typeDefs,
    resolvers
});
let app = express();
app.use('/graphql', ...helperMiddleware, apollo_server_express_1.graphqlExpress({
    schema
}));
app.get('/graphiql', apollo_server_express_1.graphiqlExpress({ endpointURL: '/graphql' })); // if you want GraphiQL enabled
app.listen(5000, () => {
    console.log("running on port 5000");
});
