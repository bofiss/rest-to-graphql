"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const axios_1 = require("axios");
const base_url = 'https://jsonplaceholder.typicode.com/';
const usersData = [
    {
        "id": 1,
        "name": "Leanne Graham",
        "username": "Bret",
        "email": "Sincere@april.biz",
        "address": {
            "street": "Kulas Light",
            "suite": "Apt. 556",
            "city": "Gwenborough",
            "zipcode": "92998-3874",
            "geo": {
                "lat": "-37.3159",
                "lng": "81.1496"
            }
        },
        "phone": "1-770-736-8031 x56442",
        "website": "hildegard.org",
        "company": {
            "name": "Romaguera-Crona",
            "catchPhrase": "Multi-layered client-server neural-net",
            "bs": "harness real-time e-markets"
        }
    },
    {
        "id": 2,
        "name": "Ervin Howell",
        "username": "Antonette",
        "email": "Shanna@melissa.tv",
        "address": {
            "street": "Victor Plains",
            "suite": "Suite 879",
            "city": "Wisokyburgh",
            "zipcode": "90566-7771",
            "geo": {
                "lat": "-43.9509",
                "lng": "-34.4618"
            }
        }
    }
];
const getUsers = () => __awaiter(this, void 0, void 0, function* () {
    const usersPromise = yield axios_1.default.get(base_url + 'users');
    const users = usersPromise.data.map((user) => __awaiter(this, void 0, void 0, function* () {
        const postsPromise = yield axios_1.default.get(base_url + 'users/' + user.id + '/posts');
        const posts = postsPromise.data.map((post) => __awaiter(this, void 0, void 0, function* () {
            const comments = yield axios_1.default.get(base_url + 'posts/' + post.id + '/comments');
            return Object.assign({}, post, { comments: comments.data });
        }));
        const albumsPromise = yield axios_1.default.get(base_url + 'users/' + user.id + '/albums');
        const albums = albumsPromise.data.map((album) => __awaiter(this, void 0, void 0, function* () {
            const photos = yield axios_1.default.get(base_url + 'albums/' + album.id + '/photos');
            return Object.assign({}, album, { photos: photos.data });
        }));
        return Object.assign({}, user, { posts: posts, albums: albums });
    }));
    return users;
});
const getUser = (root, args) => __awaiter(this, void 0, void 0, function* () {
    const user = yield axios_1.default.get(base_url + 'users/' + args.id);
    const postsPromise = yield axios_1.default.get(base_url + 'users/' + user.data.id + '/posts');
    const posts = postsPromise.data.map((post) => __awaiter(this, void 0, void 0, function* () {
        const comments = yield axios_1.default.get(base_url + 'posts/' + post.id + '/comments');
        return Object.assign({}, post, { comments: comments.data });
    }));
    const albumsPromise = yield axios_1.default.get(base_url + 'users/' + user.data.id + '/albums');
    const albums = albumsPromise.data.map((album) => __awaiter(this, void 0, void 0, function* () {
        const photos = yield axios_1.default.get(base_url + 'albums/' + album.id + '/photos');
        return Object.assign({}, album, { photos: photos.data });
    }));
    return Object.assign({}, user.data, { posts: posts, albums: albums });
});
const getPosts = () => __awaiter(this, void 0, void 0, function* () {
    const postsPromise = yield axios_1.default.get(base_url + 'posts');
    return postsPromise.data;
});
const getPost = (root, args) => __awaiter(this, void 0, void 0, function* () {
    const post = yield axios_1.default.get(base_url + 'posts/' + args.id);
    return post.data;
});
const getComments = () => __awaiter(this, void 0, void 0, function* () {
    const commentsPromise = yield axios_1.default.get(base_url + 'comments');
    return commentsPromise.data;
});
const getComment = (root, args) => __awaiter(this, void 0, void 0, function* () {
    const comment = yield axios_1.default.get(base_url + 'comments/' + args.id);
    return comment.data;
});
const getAlbums = () => __awaiter(this, void 0, void 0, function* () {
    const albumsPromise = yield axios_1.default.get(base_url + 'albums');
    const albums = albumsPromise.data.map((album) => __awaiter(this, void 0, void 0, function* () {
        const photos = yield axios_1.default.get(base_url + 'albums/' + album.id + '/photos');
        return Object.assign({}, album, { photos: photos.data });
    }));
    return albums;
});
const getAlbum = (root, args) => __awaiter(this, void 0, void 0, function* () {
    const album = yield axios_1.default.get(base_url + 'album/' + args.id);
    const photos = yield axios_1.default.get(base_url + 'albums/' + album.data.id + '/photos');
    return Object.assign({}, album.data, { photos: photos.data });
});
const getPhotos = () => __awaiter(this, void 0, void 0, function* () {
    const photosPromise = yield axios_1.default.get(base_url + 'photos');
    return photosPromise.data;
});
const getPhoto = (root, args) => __awaiter(this, void 0, void 0, function* () {
    const photo = yield axios_1.default.get(base_url + 'photos/' + args.id);
    return photo.data;
});
const createPost = (root, args) => __awaiter(this, void 0, void 0, function* () {
    console.log(args);
    const postPromise = yield axios_1.default.post(base_url + 'posts', {
        userId: args.id,
        title: args.title,
        body: args.body
    });
    console.log(postPromise.data);
    return postPromise.data;
});
const deletePost = (root, args) => __awaiter(this, void 0, void 0, function* () {
    const postPromise = yield axios_1.default.delete(base_url + 'posts/' + args.id);
    return postPromise.data;
});
const updatePost = (root, args) => __awaiter(this, void 0, void 0, function* () {
    const post = yield axios_1.default.put(base_url + 'posts/' + args.id, {
        title: args.title,
        body: args.body
    });
    return post.data;
});
exports.resolversApi = { getUsers, getUser, getPosts, getPost,
    getComments, getComment, getPhotos, getPhoto, getAlbums, getAlbum,
    createPost, deletePost, updatePost
};
